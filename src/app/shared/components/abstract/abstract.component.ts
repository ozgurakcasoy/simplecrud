import {Component, ElementRef, EventEmitter, Input, Output} from '@angular/core';
import {ControlValueAccessor} from '@angular/forms';

@Component({
  template: ''
})

export class AbstractComponent implements ControlValueAccessor {

  @Input() id: string;
  @Input() label: string;
  @Input() errorMsg: string;
  @Input() isDisabled = false; // Das Input wird von Parent Komponente verwendet.
  @Input('value') _value: any;

  @Input() get customIsInvalid() {
    return this.customIsInvalidValue;
  }

  @Output() customIsInvalidChange = new EventEmitter<boolean>();

  customIsInvalidValue: boolean;
  isRequired = false;
  name: string;
  htmlElement: HTMLElement;

  onChange = (_: any) => {
  }

  onTouched = () => {
  }

  get value() {
    return this._value;
  }

  set value(val) {
    if ((this._value && val === '') || val !== '') { // Die erste Bedingung ist für den "Feld leeren" Fall. Zweite ist für die Felder, die als leer vorbelegt werden. Dann sollen die Felder nicht sofort dirty.
      this._value = val;
      this.onChange(this._value);
    }
  }

  set customIsInvalid(val) {
    this.customIsInvalidValue = val;
    this.customIsInvalidChange.emit(this.customIsInvalidValue);
  }

  constructor(elm: ElementRef) {
    this.htmlElement = elm.nativeElement;
    this.name = this.htmlElement.getAttribute('name') || this.htmlElement.getAttribute('formControlName');

    if (this.htmlElement.getAttribute('required') !== null) {
      this.isRequired = true;
    }
    if (!this.id) {
      this.id = this.name;
    }
    if (!this.label) {
      this.label = this.name.charAt(0).toUpperCase() + this.name.slice(1);
    }
  }

  isInvalid() {
    const classes = this.htmlElement.className;
    return (this.customIsInvalid || ((classes.indexOf('ng-dirty') !== -1 && classes.indexOf('ng-touched') !== -1)
    || classes.indexOf('submitted') !== -1) && classes.indexOf('ng-invalid') !== -1);
  }

  onBlur() {
    this.onTouched();
  }

  registerOnChange(fn) {
    this.onChange = fn;
  }

  registerOnTouched(fn) {
    this.onTouched = fn;
  }

  writeValue(value: any) {
    if (value !== undefined) {
      this.value = value;
    }
  }
}
