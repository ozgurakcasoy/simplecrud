import {Component, OnInit} from '@angular/core';
import {Employee} from '../shared/models/employee';
import {EmployeeService} from '../shared/services/employee.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})
export class TableComponent implements OnInit {

  employees: Employee[] = [];
  selectedEmployee: Employee;

  constructor(private employeeService: EmployeeService, private router: Router) { }

  ngOnInit() {
    this.employeeService.getemployeees()
      .subscribe(employees => this.employees = employees);
  }

  onSelect(selectedEmployees) {
    this.selectedEmployee = selectedEmployees[0];
  }

  onClickEdit() {
    this.router.navigateByUrl('/list/' + this.selectedEmployee.id);
  }
}
