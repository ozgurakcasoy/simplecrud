import { NgModule } from '@angular/core';
import { AbstractComponent } from './components/abstract/abstract.component';
import { InputComponent } from './components/input/input.component';
import {FormsModule} from '@angular/forms';
import {EmployeeService} from './services/employee.service';
import {BrowserModule} from '@angular/platform-browser';

@NgModule({
  imports: [
    FormsModule,
    BrowserModule
  ],
  providers: [EmployeeService],
  declarations: [AbstractComponent, InputComponent],
  exports: [InputComponent]
})
export class SharedModule { }
