import {Component, Input, OnChanges, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {EmployeeService} from '../shared/services/employee.service';
import {Employee} from '../shared/models/employee';
import {Location} from '@angular/common';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {nameValidator} from './name-validator';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css']
})
export class DetailComponent implements OnInit, OnChanges {

  @Input() employee: Employee;
  employeeForm: FormGroup;
  attrWithoutForm: string;
  inline: boolean;

  constructor(private route: ActivatedRoute, private employeeService: EmployeeService, private location: Location) { }

  ngOnChanges() {
    this.inline = true;
    this.createForm();
  }

  ngOnInit(): void {
    if (!this.inline) {
      const id = +this.route.snapshot.paramMap.get('id');
      this.employeeService.getEmployee(id)
        .subscribe(employee => this.employee = employee);
    }
    this.createForm();
  }

  private createForm() {
    this.employeeForm = new FormGroup({
      // tslint:disable-next-line
      id: new FormControl(this.employee.id, [Validators.required, Validators.minLength(2)]),
      name: new FormControl(this.employee.name, [Validators.required, Validators.pattern('[A-Za-z]*'), nameValidator]),
      company: new FormControl(this.employee.company)
    });

    // this.employeeForm = this.formBuilder.group({
    //   id: [this.employee.id, [Validators.required, Validators.minLength(2)]],
    //   name: [this.employee.name, [Validators.required, nameValidator]],
    //   company: [this.employee.company]
    // });
  }

  goBack(): void {
    this.location.back();
  }

  onSubmit() {
    if (this.employeeForm.valid) {
      this.employeeService.updateEmployee(this.employeeForm.value);
      this.goBack();
    }

  }

}
