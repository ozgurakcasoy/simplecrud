import { FormControl } from '@angular/forms';

export function nameValidator(c: FormControl) {

  return (c.value.length > 3) ? null : { needsMoreChars: true };
}
