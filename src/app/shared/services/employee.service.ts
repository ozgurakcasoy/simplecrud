import {Injectable} from '@angular/core';

import {Observable} from 'rxjs/Observable';
import {of} from 'rxjs/observable/of';
import {Employee} from '../models/employee';
import {EMPLOYEES} from '../../mock-employees';
import * as _ from 'lodash';

@Injectable()
export class EmployeeService {

  employees: Employee[];

  constructor() {
    this.employees = EMPLOYEES;
  }

  getemployeees (): Observable<Employee[]> {
    return of(this.employees);
  }

  getEmployee(id: number): Observable<Employee> {
    return of(_.find(this.employees, ['id', id]));
  }

  deleteemployee (employee: Employee | number): void {
    const id = typeof employee === 'number' ? employee : employee.id;
    _.remove(this.employees, {id: id});
  }

  updateEmployee (employee: Employee): void {
    const match = _.find(this.employees, {id: employee.id});
    if (match) {
      const index = _.indexOf(this.employees, match);
      this.employees.splice(index, 1, employee);
    } else {
      this.employees.push(employee);
    }
  }
}
