import {InputComponent} from './input.component';
import {TestBed} from '@angular/core/testing';
import {Component} from '@angular/core';
import {FormsModule} from '@angular/forms';
import createSpy = jasmine.createSpy;

describe('InputComponent tests.', () => {

  let comp: InputComponent;
  let element;

  @Component({
    selector: 'app-test-wrapper-component',
    template: '<app-input name="name" inputPlaceholder="placeholder"></app-input>'
  })
  class TestWrapperComponent {
  }

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        TestWrapperComponent,
        InputComponent
      ],
      imports: [
        FormsModule
      ]
    });

    const fixture = TestBed.createComponent(TestWrapperComponent);
    comp = fixture.debugElement.children[0].componentInstance;
    element = fixture.debugElement.children[0].nativeElement;
    fixture.detectChanges();
  });

  it('input "inputPlaceholder" must be initialized', () => {
    expect(comp.inputPlaceholder).toBe('placeholder');
  });
});
