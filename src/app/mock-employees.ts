import {Employee} from './shared/models/employee';

export const EMPLOYEES: Employee[] = [
  { id: 11, name: 'Mr. Nice', company: 'Accenture'},
  { id: 12, name: 'Narco', company: 'Adidas'},
  { id: 13, name: 'Bombasto', company: 'Accenture'},
  { id: 14, name: 'Celeritas', company: 'Accenture'},
  { id: 15, name: 'Magneta', company: 'Accenture'},
  { id: 16, name: 'RubberMan', company: 'Siemens'},
  { id: 17, name: 'Dynama', company: 'HP'},
  { id: 18, name: 'Dr IQ', company: 'HP'},
  { id: 19, name: 'Magma', company: 'Datev'},
  { id: 20, name: 'Tornado', company: 'Datev'},
  { id: 21, name: 'Michael', company: 'Accenture'}
];
