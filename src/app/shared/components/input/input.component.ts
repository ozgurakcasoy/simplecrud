import {AfterViewInit, Component, ElementRef, forwardRef, Input} from '@angular/core';
import {NG_VALUE_ACCESSOR} from '@angular/forms';
import {AbstractComponent} from '../abstract/abstract.component';

@Component({
  selector: 'app-input',
  templateUrl: './input.component.html',
  styleUrls: [
    '../abstract/abstract.component.css',
    './input.component.css'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => InputComponent),
      multi: true
    }
  ]
})

export class InputComponent extends AbstractComponent implements AfterViewInit {

  @Input() inputPlaceholder: string;
  @Input() maxlength: number;

  inputElm;

  constructor(private elm: ElementRef) {
    super(elm);
  }

  ngAfterViewInit() {
    this.inputElm = this.elm.nativeElement.getElementsByTagName('input')[0];
  }
}
